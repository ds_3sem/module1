import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Buyer extends Thread {
    private final int[] lots;
    private static final AtomicInteger lotNumber = new AtomicInteger(0);
    private static final AtomicBoolean open = new AtomicBoolean(true);
    private final AtomicInteger skipLots = new AtomicInteger(0);
    private final int toSkip = 4;
    private final int numberOfBids = 10;
    private static final AtomicInteger currentBidNumber = new AtomicInteger(0);
    private static int currentBid = 0;
    private final int buyerId;
    private static final AtomicBoolean printed = new AtomicBoolean(false);
    private final CyclicBarrier barrier;
    private final Random random = new Random();

    public Buyer(int[] lots, int buyerId, CyclicBarrier barrier) {
        this.lots = lots;
        currentBid = lots[0];
        this.buyerId = buyerId + 1;
        this.barrier = barrier;
    }

    public void run() {
        while (open.get()) {
            try {
                Thread.sleep(random.nextInt(1500) + 500);

                if (currentBidNumber.get() == numberOfBids) {
                    barrier.await();
                    if (buyerId == 1 && lotNumber.get() == lots.length) {
                        open.set(false);
                    } else if (buyerId == 1) {
                        currentBidNumber.set(0);
                        lotNumber.incrementAndGet();
                        currentBid = lots[lotNumber.get()];
                        printed.set(false);
                    }
                    if (skipLots.get() > 0) {
                        skipLots.decrementAndGet();
                    }
                    barrier.await();
                } else if (skipLots.get() == 0) {
                    if(!printed.get()) {
                        System.out.println("\nLot " + (lotNumber.get()+1) + " is now ready. Opening bid is " + lots[lotNumber.get()] + "\n");
                        printed.set(true);
                    }
                    currentBidNumber.incrementAndGet();
                    currentBid += random.nextInt(50) + 10;
                    System.out.println("Buyer " + buyerId + " made a new bid - " + currentBid);
                    if (currentBidNumber.get() == numberOfBids) {
                        int sleep = random.nextInt(1750) + 500;
                        if (sleep > 1500) {
                            Thread.sleep(1500);
                            System.out.println("1...2...3...\nBuyer " + buyerId + " didn't pay. He is suspended for now.");
                            skipLots.set(toSkip);
                        } else {
                            Thread.sleep(sleep);
                            System.out.println("Buyer " + buyerId + " payed!");
                        }
                    }
                }
            } catch (InterruptedException | BrokenBarrierException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
