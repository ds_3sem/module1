import java.util.concurrent.CyclicBarrier;

public class Main {
    private static final int N = 4;
    public static void main(String[] args) throws InterruptedException {
        int[] lots = {56, 48, 64, 60, 79, 35, 92};
        CyclicBarrier barrier = new CyclicBarrier(N);
        Thread[] threads = new Thread[N];

        for (int i = 0; i < N; i++) {
            threads[i] = new Thread(new Buyer(lots, i, barrier));
            threads[i].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
    }
}