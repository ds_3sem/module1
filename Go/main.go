package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	N            = 4
	toSkip       = 4
	numberOfBids = 10
)

var (
	seed             = rand.NewSource(time.Now().UnixNano())
	random           = rand.New(seed)
	lots             = []int{56, 48, 64, 60, 79, 35, 92}
	skips            = []int{0, 0, 0, 0, 0, 0, 0}
	currentLot       = 0
	currentBidNumber = 0
	currentBid       = lots[0]
	closed           = false
	wg               sync.WaitGroup
	lock             sync.RWMutex
)

func bid(n int) {
	if skips[n] == 0 {
		for currentBidNumber < numberOfBids {
			lock.Lock()
			currentBidNumber++
			currentBid += random.Intn(40) + 10
			fmt.Printf("Buyer %d made a new bid - %d\n", n+1, currentBid)
			lock.Unlock()

			if currentBidNumber == numberOfBids {
				sleepTime := random.Intn(3) + 1
				if sleepTime > 2 {
					time.Sleep(time.Second * 3)
					fmt.Printf("1...2...3...\nBuyer %d didn't pay. He is suspended for now.\n", n+1)
					skips[n] = toSkip
				} else {
					time.Sleep(time.Second)
					fmt.Printf("Buyer %d payed!\n", n+1)
				}
			}
		}
	} else {
		skips[n] -= 1
	}
	defer wg.Done()
}

func main() {
	for {
		if closed == true {
			break
		}

		fmt.Printf("\nLot %d is now ready. Opening bid is %d\n\n", currentLot+1, lots[currentLot])
		currentLot++
		currentBidNumber = 0
		if currentLot == len(lots) {
			closed = true
		}

		wg.Add(N)

		for i := 0; i < N; i++ {
			go bid(i)
		}

		wg.Wait()
	}
}
